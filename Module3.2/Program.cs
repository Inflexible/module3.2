﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Module3_2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result) && result >= 0)
                return true;
            else
            {
                Console.WriteLine("Enter natural number");
                input = Console.ReadLine();
                return TryParseNaturalNumber(input, out result);
            }
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] fibo = new int[n];
            if (n != 0)
            {
                if (n == 1) { fibo[0] = 0; }
                else
                {
                    fibo[0] = 0;
                    fibo[1] = 1;
                    for (int i = 2; i < n; i++)
                    {
                        fibo[i] = fibo[i - 2] + fibo[i - 1];
                    }
                }
            }
            return fibo;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            char[] temp = sourceNumber.ToString().ToCharArray();
            Array.Reverse(temp);
            return int.Parse(temp);
        }
    }

    public class Task6
    {
        public int[] GenerateArray(int size)
        {
            try
            {
                return new int[size];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new int[0];
            }

        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] == 0)
                    source[i] = 0;
                else
                {
                    source[i] = -source[i];
                }
            }
            return source;
        }
    }

    public class Task7
    {
        public int[] GenerateArray(int size)
        {
            try
            {
                return new int[size];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new int[0];
            }
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> vs = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                    vs.Add(source[i]);
            }
            return vs;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] result = new int[size, size];
            Random rnd = new Random();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    result[i, j] = rnd.Next(0, 100);
                }
            }
            return result;
        }
    }
}
